#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

echo "Updating web_debug_tool"

rm -rf /opt/avena/web_debug_tool
git clone -b develop https://gitlab+deploy-token-258348:iXv88S8k52hbr79A7bns@gitlab.com/avenarobotics/web_debug_tool /opt/avena/web_debug_tool