#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/get_cameras_data
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/get_cameras_data /opt/avena/get_cameras_data
cd /opt/avena/get_cameras_data
mkdir build
cd build
cmake ..
make

cd /opt/avena/cli
ln -sf /opt/avena/get_cameras_data/build/get_cameras_data get_cameras_data