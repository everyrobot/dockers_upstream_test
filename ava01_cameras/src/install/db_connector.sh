#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/db_connector
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/db_connector /opt/avena/db_connector
cd /opt/avena/db_connector
mkdir build
cd build
cmake ..
make
make install

rm -rf /opt/avena/clear_database

cd /opt/avena/cli
ln -sf /opt/avena/db_connector/build/clear_database clear_database
