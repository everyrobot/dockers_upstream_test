#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

cd /avena/install
./db_connector.sh
./helpers.sh
./get_cameras_data.sh
