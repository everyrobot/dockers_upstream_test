#!/usr/bin/env bash
set -e

apt update
apt install -y --no-install-recommends locales curl gnupg2 lsb-release

# locale-gen en_US en_US.UTF-8
# update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
# export LANG=en_US.UTF-8

wget --no-check-certificate -O - https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
# curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -

sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

apt update
apt install -y --no-install-recommends ros-foxy-ros-base python3-pip python3-colcon-common-extensions python3-argcomplete python3-colcon-argcomplete
# pip3 install -U argcomplete

apt-get clean -y
rm -rf /var/lib/apt/lists/*

update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
echo "LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/opt/lib/lib" >> $HOME/.bashrc
echo "source /opt/ros/foxy/setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/local_setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/setup.bash" >> $HOME/.bashrc
mkdir -p ~/ros2_ws/src
cd ~/ros2_ws
# . $HOME/.bashrc
source /opt/ros/foxy/setup.bash
colcon build

# ls -l /opt/ros/foxy/setup.bash
