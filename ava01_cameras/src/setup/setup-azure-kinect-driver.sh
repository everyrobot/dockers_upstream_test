#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

# install camera drivers
apt-get update
cd /avena/debs/
# apt install -y curl software-properties-common
# curl https://packages.microsoft.com/keys/microsoft.asc |  apt-key add -
# apt-add-repository https://packages.microsoft.com/ubuntu/18.04/prod
# apt-get update


# libgl1-mesa-glx

echo libk4a1.4 libk4a1.4/accept-eula boolean true | debconf-set-selections
echo libk4a1.4 libk4a1.4/accepted-eula-hash string 0f5d5c5de396e4fee4c0753a21fee0c1ed726cf0316204edda484f08cb266d76 | debconf-set-selections -u
dpkg -i libk4a1.4*
# echo libk4a1.4 libk4a1.4/accept-eula boolean true | debconf-set-selections
# echo libk4a1.4 libk4a1.4/accepted-eula-hash string 0f5d5c5de396e4fee4c0753a21fee0c1ed726cf0316204edda484f08cb266d76 | debconf-set-selections -u
# dpkg -i libk4a1.4*

apt-get clean -y
rm -rf /var/lib/apt/lists/*
