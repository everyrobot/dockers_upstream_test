#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install build environment components"
apt-get update 

apt-get install -y --no-install-recommends git build-essential cmake xsltproc ca-certificates

apt-get install -y --no-install-recommends  libpcl-dev libboost1.71-dev nlohmann-json3-dev libopencv-dev libmysqlcppconn-dev libzmq3-dev

apt autoclean -y 
apt autoremove -y 
apt clean -y
rm -rf /var/lib/apt/lists/*