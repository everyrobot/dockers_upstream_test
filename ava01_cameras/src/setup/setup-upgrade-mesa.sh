#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

apt-get update

apt install -y software-properties-common inxi

add-apt-repository ppa:oibaf/graphics-drivers

apt-get update
# apt-get -y upgrade

apt --with-new-pkgs -y upgrade

apt autoclean -y 
apt autoremove -y 
apt clean -y
rm -rf /var/lib/apt/lists/*