use avena_db;

INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (1,'banana', 'fitcylindersegments', '{\"segment_len\": 0.05}', 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (2,'bowl', 'fitdish', NULL, 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (3,'broccoli', 'fitbroccoli', '{\"radius_max\": 0.04, \"radius_min\": 0.01, \"voxel_size\": 0.005}', 1, 0, '{\"components\":[ \"broccoli_handle\", \"broccoli_head\"]}');
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (4,'broccoli_handle', 'fitcylinder', '{\"radius_max\": 0.04, \"radius_min\": 0.01}', 0, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (5,'broccoli_head', 'fitoctomap', '{\"voxel_size\": 0.005}', 0, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (6,'cucumber', 'fitcylinder', '{\"radius_max\": 0.03, \"radius_min\": 0.01}', 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (7,'cutting_board', 'fitcuttingboard', NULL, 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (8,'handle', 'fitbox', NULL, 0, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (9,'knife', 'fitknife', NULL, 1, 0, '{\"components\":[ \"handle\", \"knife_blade\"]}');
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (10,'knife_blade', 'fitbox', NULL, 0, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (11,'lipton', 'fitbox', NULL, 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (12,'milk', 'fitbox', NULL, 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (13,'onion', 'fitoctosphere_error', NULL, 1, 0, '{\"components\":[ \"nope\", \"not_yet\"]}');
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (14,'orange', 'fitsphere', '{\"radius_max\": 0.03, \"radius_min\": 0.01}', 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (15,'plate', 'fitdish', NULL, 1, 1, NULL);
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (16,'turner', 'fitnone', NULL, 1, 0, '{\"components\":[ \"handle\", \"turner_visual\"]}');
INSERT INTO `label` (label_id, label, fit_method, fit_method_parameters, item, element, components) VALUES (17,'turner_visual', 'fitnone', NULL, 0, 1, NULL);