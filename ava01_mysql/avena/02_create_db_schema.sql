drop database if exists avena_db;
create database avena_db;
use avena_db;
set sql_mode = "ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION";

create table label
(
    label_id int not null auto_increment primary key,

    label varchar(50) not null unique,
    -- label_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00', 

    fit_method varchar(50) not null,
    -- fit_method_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    fit_method_parameters JSON, #'{"fx": 0.0, "fy": 0.0, "cx": 0.0, "cy": 0.0}'
    -- fit_method_parameters_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'

    item BIT DEFAULT 1,

    element BIT DEFAULT 1,
    
    components JSON DEFAULT null
);

create table camera_parameter 
(
    camera_parameter_id int not null auto_increment primary key,

    camera_name varchar(50) not null unique,
    camera_name_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    position JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0}'
    position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    orientation JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0, "w": 0.0}'
    orientation_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    parameters JSON, #'{"fx": 0.0, "fy": 0.0, "cx": 0.0, "cy": 0.0}'
    parameters_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

-- create table label_element
-- (
--     label_element_id int not null auto_increment primary key,

--     label_id int not null,

--     element_name varchar(50) not null,
--     element_name_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

--     fit_method varchar(50) not null,
--     fit_method_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

--     fit_method_parameters JSON, #'{"fx": 0.0, "fy": 0.0, "cx": 0.0, "cy": 0.0}'
--     fit_method_parameters_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

--     foreign key (label_id) 
-- 		references label(label_id) 
-- 		on delete cascade
-- );

create table log
(
    log_id int not null auto_increment primary key,

    log_level int not null default 0,

    log_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00' not null,

    process varchar(50) not null,

    message varchar(50) not null
);

create table scene
(
    scene_id int not null auto_increment primary key,

    camera_1_rgb longblob,
    camera_1_rgb_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    camera_1_depth longblob,
    camera_1_depth_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    camera_2_rgb longblob,
    camera_2_rgb_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    camera_2_depth longblob,
    camera_2_depth_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl_camera_1 longblob,
    pcl_camera_1_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl_camera_2 longblob,
    pcl_camera_2_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    annotated_camera_1 longblob,
    annotated_camera_1_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    annotated_camera_2 longblob,
    annotated_camera_2_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    merged_pcl longblob,
    merged_pcl_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    occupancy_grid longblob,
    occupancy_grid_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table area
(
    area_id int not null auto_increment primary key,

    area_name varchar(50),
    area_name_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    size int,
    size_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    position JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0}'
    position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    orientation JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0, "w": 0.0}'
    orientation_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl longblob,
    pcl_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    free_space JSON, #???
    free_space_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table item_cam1
(
    item_cam1_id int not null auto_increment primary key,
    scene_id int default NULL,

    mask longblob,
    mask_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    accuracy double,
    accuracy_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    label_id int default NULL,
    label_id_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    depth_data longblob,
    depth_data_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl_data longblob,
    pcl_data_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table item_cam2
(
    item_cam2_id int not null auto_increment primary key,
    scene_id int default NULL,

    mask longblob,
    mask_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    accuracy double,
    accuracy_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    label_id int default NULL,
    label_id_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    depth_data longblob,
    depth_data_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl_data longblob,
    pcl_data_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'

);

create table item
(
    item_id int not null auto_increment primary key,
    item_cam1_id int default NULL,
    item_cam2_id int default NULL,

    item_id_hash binary(32),
    item_id_hash_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    label varchar(50),
    label_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    foreign key (item_cam1_id) 
		references item_cam1(item_cam1_id) 
		on delete SET NULL,
	foreign key (item_cam2_id) 
		references item_cam2(item_cam2_id) 
		on delete SET NULL
);

create table item_element
(
    item_element_id int not null auto_increment primary key,
    item_id int default 0,

    element_label varchar(50),
    element_label_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    pcl_merged longblob,
    pcl_merged_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    element_mask_1 longblob,
    element_mask_1_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    element_mask_2 longblob,
    element_mask_2_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',
    
    element_depth_1 longblob,
    element_depth_1_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    element_depth_2 longblob,
    element_depth_2_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',
    
    element_pcl_1 longblob,
    element_pcl_1_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    element_pcl_2 longblob,
    element_pcl_2_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',
    
    primitive_shape JSON,
    primitive_shape_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',
    
    position JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0}'
    position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    orientation JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0, "w": 0.0}'
    orientation_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',
    
    foreign key (item_id) 
		references item(item_id) 
		on delete cascade
);

create table left_arm_state
(
    left_arm_state_id int not null auto_increment primary key,

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_temperature double,
    joint1_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_temperature double,
    joint2_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_temperature double,
    joint3_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_temperature double,
    joint4_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_temperature double,
    joint5_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_temperature double,
    joint6_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_temperature double,
    joint7_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_current double,
    joint1_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_current double,
    joint2_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_current double,
    joint3_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_current double,
    joint4_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_current double,
    joint5_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_current double,
    joint6_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_current double,
    joint7_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_state varchar(32),
    joint1_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_state varchar(32),
    joint2_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_state varchar(32),
    joint3_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_state varchar(32),
    joint4_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_state varchar(32),
    joint5_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_state varchar(32),
    joint6_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_state varchar(32),
    joint7_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'

);

create table right_arm_state
(
    right_arm_state_id int not null auto_increment primary key,

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_temperature double,
    joint1_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_temperature double,
    joint2_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_temperature double,
    joint3_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_temperature double,
    joint4_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_temperature double,
    joint5_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_temperature double,
    joint6_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_temperature double,
    joint7_temperature_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_current double,
    joint1_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_current double,
    joint2_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_current double,
    joint3_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_current double,
    joint4_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_current double,
    joint5_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_current double,
    joint6_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_current double,
    joint7_current_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_state varchar(32),
    joint1_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_state varchar(32),
    joint2_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_state varchar(32),
    joint3_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_state varchar(32),
    joint4_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_state varchar(32),
    joint5_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_state varchar(32),
    joint6_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_state varchar(32),
    joint7_state_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table left_arm_goal
(
    left_arm_goal_id int not null auto_increment primary key,

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'

);

create table right_arm_goal
(
    right_arm_goal_id int not null auto_increment primary key,

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'

);

create table left_arm_position
(
    left_arm_position_id int not null auto_increment primary key,

    position JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0}',
    position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    orientation JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0, "w": 0.0}'
    orientation_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table right_arm_position
(
    right_arm_position_id int not null auto_increment primary key,

    position JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0}'
    position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    orientation JSON, #'{"x": 0.0, "y": 0.0, "z": 0.0, "w": 0.0}'
    orientation_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table left_arm_path
(
    left_arm_path_id int not null auto_increment primary key,
    left_arm_position_id int,

    line_number int not null DEFAULT 0,
    line_number_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    foreign key (left_arm_position_id) 
		references left_arm_position(left_arm_position_id) 
		on delete cascade
);

create table right_arm_path
(
    right_arm_path_id int not null auto_increment primary key,
    right_arm_position_id int,

    line_number int not null,
    line_number_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    foreign key (right_arm_position_id) 
		references right_arm_position(right_arm_position_id) 
		on delete cascade
);

create table left_arm_trajectory
(
    left_arm_trajectory_id int not null auto_increment primary key,
    left_arm_position_id int,

    line_number int,
    line_number_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    delta_t double,
    delta_t_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_velocity double,
    joint1_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_velocity double,
    joint2_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_velocity double,
    joint3_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_velocity double,
    joint4_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_velocity double,
    joint5_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_velocity double,
    joint6_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_velocity double,
    joint7_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_acceleration double,
    joint1_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_acceleration double,
    joint2_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_acceleration double,
    joint3_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_acceleration double,
    joint4_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_acceleration double,
    joint5_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_acceleration double,
    joint6_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_acceleration double,
    joint7_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    foreign key (left_arm_position_id) 
		references left_arm_position(left_arm_position_id) 
		on delete cascade
);

create table right_arm_trajectory
(
    right_arm_trajectory_id int not null auto_increment primary key,
    right_arm_position_id int,

    line_number int,
    line_number_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    delta_t double,
    delta_t_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_position double,
    joint1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_position double,
    joint2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_position double,
    joint3_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_position double,
    joint4_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_position double,
    joint5_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_position double,
    joint6_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_position double,
    joint7_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_velocity double,
    joint1_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_velocity double,
    joint2_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_velocity double,
    joint3_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_velocity double,
    joint4_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_velocity double,
    joint5_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_velocity double,
    joint6_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_velocity double,
    joint7_velocity_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_acceleration double,
    joint1_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_acceleration double,
    joint2_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_acceleration double,
    joint3_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_acceleration double,
    joint4_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_acceleration double,
    joint5_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_acceleration double,
    joint6_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_acceleration double,
    joint7_acceleration_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint1_torque double,
    joint1_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint2_torque double,
    joint2_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint3_torque double,
    joint3_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint4_torque double,
    joint4_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint5_torque double,
    joint5_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint6_torque double,
    joint6_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    joint7_torque double,
    joint7_torque_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    foreign key (right_arm_position_id) 
		references right_arm_position(right_arm_position_id) 
		on delete cascade
);

create table left_gripper_state
(
    left_gripper_state_id int not null auto_increment primary key,

    matrix_from_fsr_upper_left JSON,
    matrix_from_fsr_upper_left_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_lower_left JSON,
    matrix_from_fsr_lower_left_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_upper_right JSON,
    matrix_from_fsr_upper_right_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_lower_right JSON,
    matrix_from_fsr_lower_right_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_1_position double,
    servo_1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_2_position double,
    servo_2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    current_force double,
    current_force_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    gripper_type int,
    gripper_type_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table right_gripper_state
(
    right_gripper_state_id int not null auto_increment primary key,

    matrix_from_fsr_upper_left JSON,
    matrix_from_fsr_upper_left_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_lower_left JSON,
    matrix_from_fsr_lower_left_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_upper_right JSON,
    matrix_from_fsr_upper_right_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    matrix_from_fsr_lower_right JSON,
    matrix_from_fsr_lower_right_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_1_position double,
    servo_1_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_2_position double,
    servo_2_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    current_force double,
    current_force_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    gripper_type int,
    gripper_type_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table left_gripper_goal
(
    left_gripper_goal_id int not null auto_increment primary key,

    target_force double,
    target_force_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_1_goal_position double,
    servo_1_goal_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_2_goal_position double,
    servo_2_goal_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table right_gripper_goal
(
    right_gripper_goal_id int not null auto_increment primary key,

    target_force double,
    target_force_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_1_goal_position double,
    servo_1_goal_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    servo_2_goal_position double,
    servo_2_goal_position_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table left_grasp_quality
(
    left_grasp_quality_id int not null auto_increment primary key,

    result double,
    result_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    improved_vector JSON,
    improved_vector_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);

create table right_grasp_quality
(
    right_grasp_quality_id int not null auto_increment primary key,

    result double,
    result_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00',

    improved_vector JSON,
    improved_vector_timestamp TIMESTAMP(6) default '0000-00-00 00:00:00'
);
