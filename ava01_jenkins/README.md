After building the docker:
Run jenkinsand:
Go to Manage Plugins in Top level Jenkins Configuration and select 'available' tab, type in 'cmake' and install the cmake plugin.

Go to Global Tool Configuration:
Under the CMake section:
Name: InSearchPath
Path to cmake: cmake
tag 'install automatically'
Choose install from cmake.org
Save

In job Configuration go to build section and add cmake build and:
CMake installation: InSearchPath
Source Directory: <nothing here>
Script generator: Unix Makefiles
Build type: Debug
Tag clean build
