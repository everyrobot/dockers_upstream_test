#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install build environment components"
apt-get update 

apt-get install -y git build-essential cmake xsltproc mysql-client

apt-get install -y libpcl-dev libboost1.71-dev nlohmann-json3-dev libopencv-dev libmysqlcppconn-dev libzmq3-dev

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*