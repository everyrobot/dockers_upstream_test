#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Install gnome components"
apt-get update 

apt-get install -y --no-install-recommends ubuntu-desktop gnome-terminal 
# apt-get install -y --no-install-recommends gnome-session gnome-terminal desktop-file-utils nautilus

apt-get autoclean -y 
apt-get autoremove -y 
apt-get clean -y
rm -rf /var/lib/apt/lists/*

# sed -i 's/\[daemon\]/[daemon]\nInitialSetupEnable=false/' /etc/gdm3/custom.conf