#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/generate_trajectory
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/generate_trajectory /opt/avena/generate_trajectory
cd /opt/avena/generate_trajectory
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/generate_trajectory/build/generate_trajectory generate_trajectory

exit 0
