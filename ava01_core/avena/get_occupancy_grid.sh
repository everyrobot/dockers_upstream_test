#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/get_occupancy_grid
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/get_occupancy_grid /opt/avena/get_occupancy_grid
cd /opt/avena/get_occupancy_grid
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/get_occupancy_grid/build/get_occupancy_grid get_occupancy_grid

exit 0
