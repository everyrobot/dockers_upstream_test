#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/randomize_target
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/randomize_target /opt/avena/randomize_target
cd /opt/avena/randomize_target
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/randomize_target/build/randomize_target randomize_target

exit 0
