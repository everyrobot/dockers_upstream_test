#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u
# set -o monitor        # enable script job control
# trap 'echo "child died"' CHLD

mkdir -p /opt/avena/cli

start=`date +%s`

cd /home/avena/install
echo "Updateing libraries"

./scenes.sh &
./db_connector.sh &
./helpers.sh &
wait

echo "Updateing applications"
./virtualscene.sh &
./detect.sh &
./compose_cameras_view.sh &
./estimate_shape.sh &
./spawn_shape.sh &
./get_occupancy_grid.sh &
./randomize_target.sh &
./generate_path.sh &
./generate_trajectory.sh &
wait

echo "Done"

end=`date +%s`
runtime=$((end-start))

echo "Building time " $runtime "s "

