#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/compose_cameras_view
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/compose_cameras_view /opt/avena/compose_cameras_view
cd /opt/avena/compose_cameras_view
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/compose_cameras_view/build/compose_cameras_view compose_cameras_view

exit 0
