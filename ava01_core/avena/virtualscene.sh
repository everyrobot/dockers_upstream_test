#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

# build virtaulscene
rm -rf /opt/avena/virtualscene
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/virtualscene /opt/avena/virtualscene
cd /opt/avena/virtualscene
mkdir build
cd build
cmake ..
make -j4 

# build virtaulscene coppelis saveimage plugin
rm -rf /opt/avena/coppelia_camera/programming/simExtSaveImage
cp -r /opt/avena/virtualscene/simExtSaveImage /opt/avena/coppelia_camera/programming/
cd /opt/avena/coppelia_camera/programming/simExtSaveImage
./makeit.sh

cd /opt/avena/cli
ln -sf /opt/avena/virtualscene/build/virtualscene virtualscene

exit 0
