#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/spawn_shape
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/spawn_shape /opt/avena/spawn_shape
cd /opt/avena/spawn_shape
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/spawn_shape/build/spawn_shape spawn_shape