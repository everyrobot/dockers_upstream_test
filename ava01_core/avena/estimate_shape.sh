#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/estimate_shape
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/estimate_shape /opt/avena/estimate_shape
cd /opt/avena/estimate_shape
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/estimate_shape/build/estimate_shape estimate_shape

exit 0
