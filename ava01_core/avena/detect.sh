#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/detect
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/detect /opt/avena/detect
cd /opt/avena/detect
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/detect/build/detect detect

exit 0
