#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/helpers
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/helpers /opt/avena/helpers
cd /opt/avena/helpers
mkdir build
cd build
cmake ..
make -j4 
make install

exit 0
