#!/usr/bin/env bash
### every exit != 0 fails the script
set -e
set -u

rm -rf /opt/avena/generate_path
git clone -b $GIT_BRANCH https://$GIT_USER:$GIT_PASSWORD@gitlab.com/avenarobotics/generate_path /opt/avena/generate_path
cd /opt/avena/generate_path
mkdir build
cd build
cmake ..
make -j4 

cd /opt/avena/cli
ln -sf /opt/avena/generate_path/build/generate_path generate_path
ln -sf /opt/avena/generate_path/build/generate_path_only_simulation generate_path_only_simulation

exit 0
