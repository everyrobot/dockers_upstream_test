#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Configure ROS2"

echo "LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/opt/lib/lib" >> $HOME/.bashrc
echo "source /opt/ros/foxy/setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/local_setup.bash" >> $HOME/.bashrc
echo "source ~/ros2_ws/install/setup.bash" >> $HOME/.bashrc
mkdir -p ~/ros2_ws/src
cd ~/ros2_ws
. $HOME/.bashrc
source /opt/ros/foxy/setup.bash
colcon build
