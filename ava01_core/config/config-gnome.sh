#!/usr/bin/env bash
### every exit != 0 fails the script
set -e

echo "Configure gnome components"

dbus-launch gsettings set org.gnome.desktop.lockdown disable-lock-screen true

# update-desktop-database ~/.local/share/applications/

# gsettings set org.gnome.desktop.sound event-sounds false

#dconf write /org/gnome/shell/favorite-apps "['firefox.desktop', 'gparted.desktop', 'chromium-browser.desktop', 'evolution.desktop', 'empathy.desktop', 'rhythmbox.desktop', 'shotwell.desktop', 'libreoffice-writer.desktop', 'nautilus.desktop', 'yelp.desktop']"

# Panel
# dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock autohide false
dbus-launch gsettings set org.gnome.shell favorite-apps "['brain.desktop', 'camera_avena.desktop', 'camera_franka.desktop', 'terminal_cli.desktop', 'terminal_update.desktop']"
# gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'brain.desktop']"
# gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'camera_avena.desktop']"
# gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'camera_franka.desktop']"
# gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'terminal_cli.desktop']"
# gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'terminal_update.desktop']"

# dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
# dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
# dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
# dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
# gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items true

# Wallpaper
dbus-launch gsettings set org.gnome.desktop.background picture-options 'centered'
dbus-launch gsettings set org.gnome.desktop.background picture-uri 'file:///home/avena/tool/config/avena-background-1024x768.png'
dbus-launch gsettings set org.gnome.desktop.background primary-color "#000000"
dbus-launch gsettings set org.gnome.desktop.background secondary-color "#000000"
dbus-launch gsettings set org.gnome.desktop.background color-shading-type "solid"

# Windows buttons
dbus-launch gsettings set org.gnome.desktop.wm.preferences button-layout ":minimize,maximize,close"

# gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
# gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
# gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
# gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 64
# gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items true


dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items true
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
dbus-launch gsettings set org.gnome.shell.extensions.dash-to-dock show-show-apps-button false
